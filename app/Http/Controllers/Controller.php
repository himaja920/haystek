<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function readJson(Request $request){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://api.github.com/search/repositories?q=stars:");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_USERAGENT, 'browser');
        $response = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($response,true);
        $items_star = [];
        foreach ($res['items'] as $item) {
            if($item['stargazers_count']>100) {
                $top_contributor = $this->get_top_contributor($item['full_name']);
                 $a = ['id'=>$item['id'], 'name'=> $item['name'], 'Owner object' => $item['owner'], 'html_url'=> $item['html_url'],
                    'Created_at'=> date('F jS Y,h:i:s a',strtotime($item['created_at']))];
                $a['Contributor'] = $top_contributor;
                array_push($items_star, $a);
            }
        }
        return $items_star;
    }
    public function get_top_contributor($repo_url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://api.github.com/repos/". $repo_url ."/contributors");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_USERAGENT, 'browser');
        $response = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($response,true);
        $top_contributor = ['login'=>$res[0]['login'],'id'=>$res[0]['id'],'Contributions'=>$res[0]['contributions']];
        return $top_contributor;

    }
}
